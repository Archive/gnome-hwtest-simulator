const Gio = imports.gi.Gio;
const Lang = imports.lang;
const Mainloop = imports.mainloop;

const FirewallDZoneIface = '\
<node>\
  <interface name="org.fedoraproject.FirewallD1.zone">\
    <method name="changeZone">\
      <arg direction="in"  type="s" name="zone" />\
      <arg direction="in"  type="s" name="interface" />\
      <arg direction="out" type="s" />\
    </method>\
    <signal name="IcmpBlockRemoved">\
      <arg type="s" name="zone" />\
      <arg type="s" name="icmp" />\
    </signal>\
    <method name="getInterfaces">\
      <arg direction="in"  type="s" name="zone" />\
      <arg direction="out" type="as" />\
    </method>\
    <method name="removeService">\
      <arg direction="in"  type="s" name="zone" />\
      <arg direction="in"  type="s" name="service" />\
      <arg direction="out" type="s" />\
    </method>\
    <method name="removeInterface">\
      <arg direction="in"  type="s" name="zone" />\
      <arg direction="in"  type="s" name="interface" />\
      <arg direction="out" type="s" />\
    </method>\
    <signal name="MasqueradeRemoved">\
      <arg type="s" name="zone" />\
    </signal>\
    <method name="queryPort">\
      <arg direction="in"  type="s" name="zone" />\
      <arg direction="in"  type="s" name="port" />\
      <arg direction="in"  type="s" name="protocol" />\
      <arg direction="out" type="b" />\
    </method>\
    <method name="addMasquerade">\
      <arg direction="in"  type="s" name="zone" />\
      <arg direction="in"  type="i" name="timeout" />\
      <arg direction="out" type="s" />\
    </method>\
    <method name="getRichRules">\
      <arg direction="in"  type="s" name="zone" />\
      <arg direction="out" type="as" />\
    </method>\
    <method name="getPorts">\
      <arg direction="in"  type="s" name="zone" />\
      <arg direction="out" type="aas" />\
    </method>\
    <signal name="PortAdded">\
      <arg type="s" name="zone" />\
      <arg type="s" name="port" />\
      <arg type="s" name="protocol" />\
      <arg type="i" name="timeout" />\
    </signal>\
    <method name="addService">\
      <arg direction="in"  type="s" name="zone" />\
      <arg direction="in"  type="s" name="service" />\
      <arg direction="in"  type="i" name="timeout" />\
      <arg direction="out" type="s" />\
    </method>\
    <method name="queryInterface">\
      <arg direction="in"  type="s" name="zone" />\
      <arg direction="in"  type="s" name="interface" />\
      <arg direction="out" type="b" />\
    </method>\
    <method name="getServices">\
      <arg direction="in"  type="s" name="zone" />\
      <arg direction="out" type="as" />\
    </method>\
    <signal name="RichRuleAdded">\
      <arg type="s" name="zone" />\
      <arg type="s" name="rule" />\
      <arg type="i" name="timeout" />\
    </signal>\
    <method name="queryService">\
      <arg direction="in"  type="s" name="zone" />\
      <arg direction="in"  type="s" name="service" />\
      <arg direction="out" type="b" />\
    </method>\
    <signal name="RichRuleRemoved">\
      <arg type="s" name="zone" />\
      <arg type="s" name="rule" />\
    </signal>\
    <signal name="SourceRemoved">\
      <arg type="s" name="zone" />\
      <arg type="s" name="source" />\
    </signal>\
    <method name="isImmutable">\
      <arg direction="in"  type="s" name="zone" />\
      <arg direction="out" type="b" />\
    </method>\
    <method name="addIcmpBlock">\
      <arg direction="in"  type="s" name="zone" />\
      <arg direction="in"  type="s" name="icmp" />\
      <arg direction="in"  type="i" name="timeout" />\
      <arg direction="out" type="s" />\
    </method>\
    <method name="addSource">\
      <arg direction="in"  type="s" name="zone" />\
      <arg direction="in"  type="s" name="source" />\
      <arg direction="out" type="s" />\
    </method>\
    <signal name="ZoneChanged">\
      <arg type="s" name="zone" />\
      <arg type="s" name="interface" />\
    </signal>\
    <method name="querySource">\
      <arg direction="in"  type="s" name="zone" />\
      <arg direction="in"  type="s" name="source" />\
      <arg direction="out" type="b" />\
    </method>\
    <method name="addPort">\
      <arg direction="in"  type="s" name="zone" />\
      <arg direction="in"  type="s" name="port" />\
      <arg direction="in"  type="s" name="protocol" />\
      <arg direction="in"  type="i" name="timeout" />\
      <arg direction="out" type="s" />\
    </method>\
    <method name="queryIcmpBlock">\
      <arg direction="in"  type="s" name="zone" />\
      <arg direction="in"  type="s" name="icmp" />\
      <arg direction="out" type="b" />\
    </method>\
    <method name="removeIcmpBlock">\
      <arg direction="in"  type="s" name="zone" />\
      <arg direction="in"  type="s" name="icmp" />\
      <arg direction="out" type="s" />\
    </method>\
    <signal name="InterfaceRemoved">\
      <arg type="s" name="zone" />\
      <arg type="s" name="interface" />\
    </signal>\
    <method name="getZoneOfInterface">\
      <arg direction="in"  type="s" name="interface" />\
      <arg direction="out" type="s" />\
    </method>\
    <method name="changeZoneOfInterface">\
      <arg direction="in"  type="s" name="zone" />\
      <arg direction="in"  type="s" name="interface" />\
      <arg direction="out" type="s" />\
    </method>\
    <signal name="ServiceAdded">\
      <arg type="s" name="zone" />\
      <arg type="s" name="service" />\
      <arg type="i" name="timeout" />\
    </signal>\
    <method name="removeSource">\
      <arg direction="in"  type="s" name="zone" />\
      <arg direction="in"  type="s" name="source" />\
      <arg direction="out" type="s" />\
    </method>\
    <method name="removeRichRule">\
      <arg direction="in"  type="s" name="zone" />\
      <arg direction="in"  type="s" name="rule" />\
      <arg direction="out" type="s" />\
    </method>\
    <signal name="SourceAdded">\
      <arg type="s" name="zone" />\
      <arg type="s" name="source" />\
    </signal>\
    <method name="changeZoneOfSource">\
      <arg direction="in"  type="s" name="zone" />\
      <arg direction="in"  type="s" name="source" />\
      <arg direction="out" type="s" />\
    </method>\
    <method name="getForwardPorts">\
      <arg direction="in"  type="s" name="zone" />\
      <arg direction="out" type="aas" />\
    </method>\
    <signal name="ZoneOfInterfaceChanged">\
      <arg type="s" name="zone" />\
      <arg type="s" name="interface" />\
    </signal>\
    <signal name="ZoneOfSourceChanged">\
      <arg type="s" name="zone" />\
      <arg type="s" name="source" />\
    </signal>\
    <signal name="InterfaceAdded">\
      <arg type="s" name="zone" />\
      <arg type="s" name="interface" />\
    </signal>\
    <signal name="ServiceRemoved">\
      <arg type="s" name="zone" />\
      <arg type="s" name="service" />\
    </signal>\
    <method name="getIcmpBlocks">\
      <arg direction="in"  type="s" name="zone" />\
      <arg direction="out" type="as" />\
    </method>\
    <method name="queryForwardPort">\
      <arg direction="in"  type="s" name="zone" />\
      <arg direction="in"  type="s" name="port" />\
      <arg direction="in"  type="s" name="protocol" />\
      <arg direction="in"  type="s" name="toport" />\
      <arg direction="in"  type="s" name="toaddr" />\
      <arg direction="out" type="b" />\
    </method>\
    <method name="getSources">\
      <arg direction="in"  type="s" name="zone" />\
      <arg direction="out" type="as" />\
    </method>\
    <method name="getZones">\
      <arg direction="out" type="as" />\
    </method>\
    <method name="addRichRule">\
      <arg direction="in"  type="s" name="zone" />\
      <arg direction="in"  type="s" name="rule" />\
      <arg direction="in"  type="i" name="timeout" />\
      <arg direction="out" type="s" />\
    </method>\
    <signal name="MasqueradeAdded">\
      <arg type="s" name="zone" />\
      <arg type="i" name="timeout" />\
    </signal>\
    <signal name="IcmpBlockAdded">\
      <arg type="s" name="zone" />\
      <arg type="s" name="icmp" />\
      <arg type="i" name="timeout" />\
    </signal>\
    <signal name="ForwardPortRemoved">\
      <arg type="s" name="zone" />\
      <arg type="s" name="port" />\
      <arg type="s" name="protocol" />\
      <arg type="s" name="toport" />\
      <arg type="s" name="toaddr" />\
    </signal>\
    <signal name="PortRemoved">\
      <arg type="s" name="zone" />\
      <arg type="s" name="port" />\
      <arg type="s" name="protocol" />\
    </signal>\
    <method name="queryMasquerade">\
      <arg direction="in"  type="s" name="zone" />\
      <arg direction="out" type="b" />\
    </method>\
    <method name="removeMasquerade">\
      <arg direction="in"  type="s" name="zone" />\
      <arg direction="out" type="s" />\
    </method>\
    <signal name="ForwardPortAdded">\
      <arg type="s" name="zone" />\
      <arg type="s" name="port" />\
      <arg type="s" name="protocol" />\
      <arg type="s" name="toport" />\
      <arg type="s" name="toaddr" />\
      <arg type="i" name="timeout" />\
    </signal>\
    <method name="removeForwardPort">\
      <arg direction="in"  type="s" name="zone" />\
      <arg direction="in"  type="s" name="port" />\
      <arg direction="in"  type="s" name="protocol" />\
      <arg direction="in"  type="s" name="toport" />\
      <arg direction="in"  type="s" name="toaddr" />\
      <arg direction="out" type="s" />\
    </method>\
    <method name="getZoneOfSource">\
      <arg direction="in"  type="s" name="source" />\
      <arg direction="out" type="s" />\
    </method>\
    <method name="queryRichRule">\
      <arg direction="in"  type="s" name="zone" />\
      <arg direction="in"  type="s" name="rule" />\
      <arg direction="out" type="b" />\
    </method>\
    <method name="addInterface">\
      <arg direction="in"  type="s" name="zone" />\
      <arg direction="in"  type="s" name="interface" />\
      <arg direction="out" type="s" />\
    </method>\
    <method name="addForwardPort">\
      <arg direction="in"  type="s" name="zone" />\
      <arg direction="in"  type="s" name="port" />\
      <arg direction="in"  type="s" name="protocol" />\
      <arg direction="in"  type="s" name="toport" />\
      <arg direction="in"  type="s" name="toaddr" />\
      <arg direction="in"  type="i" name="timeout" />\
      <arg direction="out" type="s" />\
    </method>\
    <method name="removePort">\
      <arg direction="in"  type="s" name="zone" />\
      <arg direction="in"  type="s" name="port" />\
      <arg direction="in"  type="s" name="protocol" />\
      <arg direction="out" type="s" />\
    </method>\
    <method name="getActiveZones">\
      <arg direction="out" type="a{sa{sas}}" />\
    </method>\
  </interface>\
</node>';

const FirewallDZone = Gio.DBusProxy.makeProxyWrapper(FirewallDZoneIface);

function setupFirewallRules() {
    let zoneProxy = new FirewallDZone(Gio.DBus.system,
                                      'org.fedoraproject.FirewallD1',
                                      '/org/fedoraproject/FirewallD1');
    let [rules] = zoneProxy.getRichRulesSync('internal');
    for (let rule of rules) {
        if (/source address="10.0.3.0\/24"/.test(rule)) {
            zoneProxy.removeRichRuleSync('internal', rule);
        }
    }

    function addRule(rule) {
        zoneProxy.addRichRuleSync('internal', rule, 0);
    }

//    addRule('rule family="ipv4" source address="10.0.3.0/24" forward-port port="80" protocol="tcp" to-port="51269"');
//    addRule('rule family="ipv4" source address="10.0.3.0/24" port port="80" protocol="tcp" accept');
    addRule('rule family="ipv4" source address="10.0.3.0/24" port port="3260" protocol="tcp" accept');
    addRule('rule family="ipv4" source address="10.0.3.0/24" port port="35000-35015" protocol="tcp" accept');
    addRule('rule family="ipv4" source address="10.0.3.0/24" protocol value="udp" accept');
}

function nmcli() {
    let args = ['nmcli', 'conn']
    args.push.apply(args, arguments);
    let process = Gio.Subprocess.new(args, Gio.SubprocessFlags.NONE)
    process.wait (null);
    let status = process.get_exit_status();
    if (status != 0) {
        throw new Error("Call to nmcli failed");
    }
}

function nmcliIgnore() {
    let args = ['nmcli', 'conn']
    args.push.apply(args, arguments);
    let process = Gio.Subprocess.new(args,
                                     Gio.SubprocessFlags.STDIO_SILENCE | Gio.SubprocessFlags.STDERR_SILENCE);
    process.wait (null);
}

function setupBridge() {
    nmcliIgnore('delete', 'hwtestbr');
    nmcliIgnore('delete', 'hwtestbr-eth');
    nmcli('add', 'type', 'bridge', 'ifname', 'hwtestbr', 'con-name', 'hwtestbr', 'stp', 'no', 'ip4', '10.0.3.3/24');
//    nmcli('modify', 'hwtestbr', 'ipv4.method', 'disabled');
    nmcli('modify', 'hwtestbr', 'ipv6.method', 'ignore');
//    nmcli('modify', 'hwtestbr', 'connection.zone', 'internal');
    nmcli('modify', 'hwtestbr', 'connection.zone', 'trusted');
//    nmcli('add', 'type', 'bridge-slave', 'con-name', 'hwtestbr-eth', 'ifname', 'enp0s20u2c2', 'master', 'hwtestbr');
//    nmcli('modify', 'hwtestbr-eth', '802-3-ethernet.mac-address', '9c:eb:e8:11:a1:be');
    nmcli('up', 'hwtestbr');
//    nmcli('up', 'hwtestbr-eth');
}

const NetworkManager = new Lang.Class({
    Name: 'NetworkManager',

    _init: function(application) {
        this.application = application;
        setupBridge();
        setupFirewallRules();
    },

    getNetworkArgs: function(vlanId, hwAddr, listen) {
        let args = ['-net','nic,vlan=' + vlanId + ',macaddr='+hwAddr];
//        let args = ['-net','nic,vlan=' + vlanId + ',macaddr='+hwAddr+',model=virtio'];
//         let type = listen ? 'listen' : 'connect';
        // args.push('-net', 'socket,vlan=' + vlanId + ',' + type + '=127.0.0.1:35431');
        args.push('-net', 'bridge,vlan=' + vlanId + ',' + 'br=hwtestbr,helper=/home/otaylor/tmp/bridge-helper/bridge-helper');
        return args;
    }
});
