#!/usr/bin/gjs

const Gdk = imports.gi.Gdk;
const Gio = imports.gi.Gio;
const GLib = imports.gi.GLib;
const GObject = imports.gi.GObject;
const Gtk = imports.gi.Gtk;
const Lang = imports.lang;
const PangoCairo = imports.gi.PangoCairo;
const Signals = imports.signals;
const SpiceClientGLib = imports.gi.SpiceClientGLib;
const SpiceClientGtk = imports.gi.SpiceClientGtk;
const Vte = imports.gi.Vte;

const Machine = imports.machine;

Gtk.init(null, null);

let bootTypeMap = {
    harddrive: Machine.BOOT_HARDDRIVE,
    network: Machine.BOOT_NETWORK,
    image: Machine.BOOT_IMAGE
};

const StraightJacket = new Lang.Class({
    Name: 'StraightJacket',
    Extends: Gtk.Bin,

    vfunc_get_preferred_width: function() {
        return [1, 320];
    },

    vfunc_get_preferred_height: function() {
        return [1, 240];
    }
});

const PlaceHolder = new Lang.Class({
    Name: 'PlaceHolder',
    Extends: Gtk.DrawingArea,

    _init: function() {
        this.parent();
        this._text = null;
    },

    setText: function(text) {
        this._text = text;
        this.queue_draw();
    },

    vfunc_draw: function(context) {
        let markup = '<span color="#aaaaaa" size="48000">' + this._text + '</span>';

        let layout = this.create_pango_layout("");
        layout.set_markup(markup, -1);

        let allocation = this.get_allocation();
        context.setSourceRGB(0, 0, 0);
        context.paint();

        let pcc = PangoCairo.create_context(context);
        let [, logical] = layout.get_pixel_extents();

        context.moveTo((allocation.width - logical.width) / 2,
                       (allocation.height - logical.height) / 2);

        PangoCairo.show_layout(context, layout);
    }
});

const Indicator = new Lang.Class({
    Name: 'Indicator',
    Extends: Gtk.Bin,

    _init: function(proxy) {
        this.parent();

        this.proxy = proxy;
        this.proxy.connect('mode-changed', this._update.bind(this));
        this.proxy.connect('auto-on-changed', this._update.bind(this));
    },

    _update: function(proxy) {
        this.queue_draw();
    },

    vfunc_get_preferred_width: function() {
        return [24, 24];
    },

    vfunc_get_preferred_height: function() {
        return [24, 24];
    },

    vfunc_draw: function(context) {
        let strokeColor;
        let fillColor;

        if (this.proxy.mode == Machine.MODE_AUTO) {
            if (this.proxy.on) {
                strokeColor = [0, 0.7, 0, 1.0];
                fillColor = [0, 0.7, 0, 1.0];
            } else {
                strokeColor = [0.7, 0.7, 0, 1.0];
                fillColor = [0.7, 0.7, 0, 0.7];
            }
        } else {
            if (this.proxy.on) {
                strokeColor = [0.7, 0.0, 0, 1.0];
                fillColor = [0.7, 0.0, 0, 0.7];
            } else {
                strokeColor = [0.3, 0.3, 0.3, 1.0];
                fillColor = [0.3, 0.3, 0.3, 0.7];
            }
        }

        this.parent(context);

        let allocation = this.get_allocation();
        let radius = Math.min(allocation.height, allocation.width) / 2 - 3;

        context.setSourceRGBA.apply(context, fillColor);
        context.arc(allocation.width / 2, allocation.height / 2, radius, 0, 2 * Math.PI)
        context.fillPreserve();
        context.setSourceRGBA.apply(context, strokeColor);
        context.stroke();
    },
});


const MachineWidget = new Lang.Class({
    Name: 'MachineWidget',
    Extends: Gtk.Box,

    _init: function(mainView, proxy) {
        this.parent();

        this.mainView = mainView;
        this.showSerial = false;

        this.proxy = proxy;
        this.proxy.connect('state-changed',
                           this._onProxyStateChanged.bind(this));

        /////////////////////

        this.actions = new Gio.SimpleActionGroup();

        let action;

        action = new Gio.SimpleAction({ name: 'save-terminal-contents' });
        action.connect('activate', this._onSaveTerminalContents.bind(this));
        this.actions.add_action(action);

        let bootTypeString;
        for (let k in bootTypeMap) {
            if (bootTypeMap[k] == this.proxy.bootType)
                bootTypeString = k;
        }

        action = new Gio.SimpleAction({ name: 'boot-type',
                                        parameter_type: GLib.VariantType.new('s'),
                                        state: GLib.Variant.new('s', bootTypeString) });
        action.connect('notify::state', this._onBootTypeChanged.bind(this));
        this.actions.add_action(action);

        this.builder = new Gtk.Builder();
        let content = "<interface>" +
                      "  <menu id='machine-menu'>" +
                      "    <section>" +
                      "      <item>" +
                      "        <attribute name='label'>Boot from Hard Drive</attribute>" +
                      "        <attribute name='action'>machine.boot-type</attribute>" +
                      "        <attribute name='target'>harddrive</attribute>" +
                      "      </item>" +
                      "      <item>" +
                      "        <attribute name='label'>Boot from Network</attribute>" +
                      "        <attribute name='action'>machine.boot-type</attribute>" +
                      "        <attribute name='target'>network</attribute>" +
                      "      </item>" +
                      "      <item>" +
                      "        <attribute name='label'>Boot from Image</attribute>" +
                      "        <attribute name='action'>machine.boot-type</attribute>" +
                      "        <attribute name='target'>image</attribute>" +
                      "      </item>" +
                      "    </section>" +
                      "  </menu>" +
                      "  <menu id='terminal-menu'>" +
                      "    <section>" +
                      "      <item>" +
                      "        <attribute name='label'>Save contents</attribute>" +
                      "        <attribute name='action'>machine.save-terminal-contents</attribute>" +
                      "      </item>" +
                      "    </section>" +
                      "  </menu>" +
                      "</interface>";
        this.builder.add_from_string(content, content.length);

        //////////////////////////////////

        this.set_orientation(Gtk.Orientation.VERTICAL);

        let header = new Gtk.Box({ orientation: Gtk.Orientation.HORIZONTAL });

        this.indicator = new Indicator(proxy);
        header.pack_start(this.indicator, false, false, 0);

        let buttonBox = new Gtk.Box({ orientation: Gtk.Orientation.HORIZONTAL,
                                      homogeneous: true });
        buttonBox.get_style_context().add_class("linked");

        header.pack_start(buttonBox, false, false, 0);

        let button;
        button = new Gtk.RadioButton({ label: 'Off', draw_indicator: false,  });
        if (proxy.mode == Machine.MODE_OFF)
            button.set_active(true);
        button.connect('toggled', this._onModeToggled.bind(this, Machine.MODE_OFF));
        buttonBox.pack_start(button, true, true, 0);
        if (proxy.hasAuto) {
            button = new Gtk.RadioButton({ label: 'Auto', draw_indicator: false, group: button });
            if (proxy.mode == Machine.MODE_AUTO)
                button.set_active(true);
            button.connect('toggled', this._onModeToggled.bind(this, Machine.MODE_AUTO));
            buttonBox.pack_start(button, true, true, 0);
        }
        button = new Gtk.RadioButton({ label: 'On', draw_indicator: false, group: button });
        if (proxy.mode == Machine.MODE_ON)
            button.set_active(true);
        button.connect('toggled', this._onModeToggled.bind(this, Machine.MODE_ON));
        buttonBox.pack_start(button, true, true, 0);

        let label = new Gtk.Label({ label: this.proxy.name });
        header.pack_start(label, true, true, 0);

        let gearsButton = new Gtk.MenuButton();
        let image = new Gtk.Image({ icon_name: 'emblem-system-symbolic' });
        gearsButton.add(image);
        gearsButton.insert_action_group("machine", this.actions);

        this.powerButton = new Gtk.Button();
        header.pack_start(this.powerButton, false, false, 0);
        let image = new Gtk.Image({ icon_name: 'system-shutdown-symbolic' });
        this.powerButton.add(image);
        this.powerButton.connect('button-press-event', this._onPowerButtonPress.bind(this));
        this.powerButton.connect_after('button-release-event', this._onPowerButtonRelease.bind(this));
        this.powerButton.connect('clicked', this._onPowerButtonClicked.bind(this));

        gearsButton.set_menu_model(this.builder.get_object("machine-menu"));

        header.pack_start(gearsButton, false, false, 0);

        this.pack_start(header, false, false, 0);
        header.show_all();

        this.eventbox = new Gtk.EventBox({ visible_window: false });
        this.eventbox.add_events(Gdk.EventMask.BUTTON_PRESS_MASK);
        this.eventbox.connect('button-press-event', this.popOut.bind(this));

        this.pack_start(this.eventbox, false, false, 0);
        this.eventbox.show();
        this.eventbox.set_size_request(320, 240);

        this.straightjacket = new StraightJacket();
        this.straightjacket.show();
        this.eventbox.add(this.straightjacket);

        this.placeholder = new PlaceHolder();
        this.placeholder.setText('Off');
        this.straightjacket.add(this.placeholder);

        this.poppedOut = false;
        this._onProxyStateChanged(this.proxy);
    },

    popOut: function() {
        if (this.machine == null || this.poppedOut)
            return;

        if (this.mainView.machineWidget != null)
            this.mainView.machineWidget.popIn();

        this.straightjacket.remove(this.display);
        this.straightjacket.add(this.placeholder);
        this.eventbox.set_above_child(false);
        this.placeholder.setText('>');
        this.mainView.setDisplay(this);
        if (this.mainView.showSerial)
            this.term.grab_focus();
        else
            this.display.grab_focus();
        this.poppedOut = true;
    },

    popIn: function() {
        if (!this.poppedOut)
            return;

        this.mainView.setDisplay(null);
        this.straightjacket.remove(this.placeholder);
        this.straightjacket.add(this.display);
        this.eventbox.set_above_child(true);
        this.poppedOut = false;
    },

    _onProxyStateChanged: function(proxy) {
//        this.powerButton.set_active(proxy.state != Machine.MACHINE_OFF);
        this.powerButton.set_sensitive(proxy.hasPower() &&
                                       (proxy.state == Machine.MACHINE_ON || proxy.state == Machine.MACHINE_OFF));

        if (proxy.state == Machine.MACHINE_ON && this.machine == null)
            this._attach(proxy.machine);
        else if (proxy.state == Machine.MACHINE_OFF && this.machine != null)
            this._detach();
    },

    _attach: function(machine) {
        this.machine = machine;

        let session = new SpiceClientGLib.Session({ uri: "spice://127.0.0.1?port=" + machine.spicePort });
        session.password = machine.spicePassword;

        // connect_after is used, because session.connect() is shadowed.
        session.connect_after('channel-new', this._onChannelNew.bind(this));
        session.connect_after('channel-destroy', this._onChannelDestroy.bind(this));

        if (!session.connect())
            printerr("Could not connect");
    },

    _detach: function() {
        if (this.display) {
            if (this.poppedOut)
                this.popIn();

            this.display.destroy();
            this.display = null;
            this.term.destroy();
            this.term = null;
            this.straightjacket.add(this.placeholder);
            this.eventbox.set_above_child(false);
            this.placeholder.setText('Off');
        }

        // FIXME: disconnect, or dispose the session object

        this.machine = null;
    },

    _onChannelNew: function(session, channel) {
        if (channel instanceof SpiceClientGLib.DisplayChannel) {
            this.display = SpiceClientGtk.Display.new(session, channel.channel_id);
            this.display.resize_guest = true;
            this.display.grab_keyboard = false;
            this.display.grab_mouse = false;

            this.display.show();
            this.straightjacket.remove(this.placeholder);
            this.straightjacket.add(this.display);
            this.eventbox.set_above_child(true);
            channel.connect();

            this.term = new Vte.Terminal();
            this.term.set_scrollback_lines(-1);
            this.term.show();
            this.term.insert_action_group("machine", this.actions);

            this.term.connect('button-press-event',
                              this._onTerminalButtonPress.bind(this));

            let m = /pty:(.*)/.exec(this.machine.serialDevices['serial0']);
            let pty = m[1];
            let libexecdir = pkg.libdir + '/../libexec';
            let splice = libexecdir + '/gnome-hwtest-terminal-splice';
            this.term.spawn_sync (Vte.PtyFlags.NO_HELPER,
                                  '.',
                                  [splice, pty],
                                  null,
                                  GLib.SpawnFlags.DEFAULT,
                                  null, null);
            this.term.set_default_colors();

            if (this.mainView.machineWidget == null)
                this.popOut();
        }
    },

    _onChannelDestroy: function(session, channel) {
    },

    _onModeToggled: function(mode, button) {
        if (button.active)
            this.proxy.setMode(mode);
    },

    _onBootTypeChanged: function(action) {
        let [bootTypeString,] = action.get_state().get_string();
        this.proxy.setBootType(bootTypeMap[bootTypeString]);
    },

    _onPowerButtonClicked: function() {
        if (this.proxy.on) {
            let currentEvent = Gtk.get_current_event();
            let longPress = (this._powerButtonPressTime != null &&
                             currentEvent &&
                             (currentEvent.get_time() - this._powerButtonPressTime > 4000));
            if (longPress)
                this.proxy.hardOff();
            else
                this.proxy.requestOff();
        } else {
            this.proxy.boot();
        }
    },

    _onPowerButtonPress: function(buttonWidget, event) {
        let [, button] = event.get_button();
        if (button == 1)
            this._powerButtonPressTime = event.get_time();
    },

    _onPowerButtonRelease: function(buttonWidget, event) {
        let [, button] = event.get_button();
        if (button == 1)
            this._powerButtonPressTime = null;
    },

    _onTerminalButtonPress: function(terminal, event) {
        let [, button] = event.get_button();
        if (button == 3) {
            let popover = Gtk.Popover.new_from_model(this.term,
                                                     this.builder.get_object("terminal-menu"));
            let [,x,y] = event.get_coords();
            let rect = terminal.get_allocation();
            rect.x = x;
            rect.y = y;
            rect.width = 0;
            rect.height = 0;
            popover.set_pointing_to(rect);
            popover.show();

            return true;
        }

        return false;
    },

    _onSaveTerminalContents: function() {
        outputFile = this.mainView.application.getHomeFile(this.proxy.name + '-serial.txt');
        output = outputFile.replace(null, false,
                                    Gio.FileCreateFlags.NONE,
                                    null);
        this.term.write_contents (output, Vte.TerminalWriteFlags.DEFAULT,
                                  null);
        output.close(null);
        print("Saved terminal contents");
    }
});
Signals.addSignalMethods(MachineWidget.prototype);

const MainView = new Lang.Class({
    Name: 'MainView',
    Extends: Gtk.Stack,

    Signals: {
        'display-changed': {},
        'show-serial-changed': {}
    },

    _init: function(application) {
        this.parent();

        this.application = application;

        this.consoleHolder = new StraightJacket();
        this.consoleHolder.show();
        this.add_named(this.consoleHolder, 'console');

        this.serialHolder = new StraightJacket();
        this.serialHolder.show();
        this.add_named(this.serialHolder, 'serial');

        this.showSerial = false;
    },

    setDisplay: function(machineWidget) {
        if (this.machineWidget) {
            this.consoleHolder.remove(this.machineWidget.display);
            this.serialHolder.remove(this.machineWidget.term);
        }

        this.machineWidget = machineWidget;

        if (this.machineWidget) {
            this.consoleHolder.add(this.machineWidget.display);
            this.serialHolder.add(this.machineWidget.term);
            this.setShowSerial(machineWidget.showSerial);
        }

        this.emit('display-changed');
    },

    setShowSerial: function(showSerial) {
        showSerial = !!showSerial;
        if (showSerial != this.showSerial) {
            this.showSerial = showSerial;
            this.set_visible_child_name(showSerial ? 'serial' : 'console');
            this.get_visible_child().get_child().grab_focus();
            if (this.machineWidget)
                this.machineWidget.showSerial = showSerial;
            this.emit('show-serial-changed');
        }
    }
});
