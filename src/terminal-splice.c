#include <errno.h>
#include <termios.h>
#include <fcntl.h>

#include <gio/gio.h>
#include <gio/gunixinputstream.h>
#include <gio/gunixoutputstream.h>
#include <stdlib.h>

static void
check_error(const char *where, GError *error)
{
    if (error != NULL) {
        g_printerr("Error %s: %s\n", where, error->message);
        exit(1);
    }
}

static void
die_errno(const char *where)
{
    g_printerr("Error %s: %s\n", where, g_strerror(errno));
    exit(1);
}

static void
on_eof(GObject      *source_object,
       GAsyncResult *result,
       gpointer      data)
{
    GMainLoop *loop = data;
    GError *error = NULL;
    g_output_stream_splice_finish(G_OUTPUT_STREAM(source_object), result, &error);
    check_error("Finishing splice", error);

    g_main_loop_quit(loop);
}

void
make_raw(int fd)
{
    struct termios options;

    if (tcgetattr(fd, &options) == -1)
        die_errno("Getting TTY options");

    cfmakeraw(&options);

    if (tcsetattr(fd, TCSANOW, &options) == -1)
        die_errno("Setting TTY to raw mode");
}

int main(int argc, char **argv) {
    const char *pty_path;
    int pty_in_fd, pty_out_fd;
    GInputStream *stdin, *instream;
    GOutputStream *stdout, *outstream;
    GMainLoop *loop;
    GError *error = NULL;

    if (argc != 2) {
        g_printerr("Usage: gnome-hwtest-terminal-splice <pty>\n");
        exit(1);
    }

    pty_path = argv[1];

    pty_in_fd = open(pty_path, O_RDONLY);
    if (pty_in_fd == -1)
        die_errno("Opening PTY for reading");

    pty_out_fd = open(pty_path, O_WRONLY);
    if (pty_out_fd == -1)
        die_errno("Opening PTY for writing");

    make_raw (0);
    make_raw (pty_in_fd);

    stdin = g_unix_input_stream_new(0, FALSE);
    stdout = g_unix_output_stream_new(1, FALSE);
    instream = g_unix_input_stream_new(pty_in_fd, FALSE);
    outstream = g_unix_output_stream_new(pty_out_fd, FALSE);

    loop = g_main_loop_new (NULL, FALSE);

    g_output_stream_splice_async(stdout, instream,
                                 G_OUTPUT_STREAM_SPLICE_NONE,
                                 G_PRIORITY_DEFAULT, NULL,
                                 on_eof, loop);
    check_error("Splicing stdout to PTY", error);

    g_output_stream_splice_async(outstream, stdin,
                                 G_OUTPUT_STREAM_SPLICE_NONE,
                                 G_PRIORITY_DEFAULT, NULL,
                                 on_eof, loop);
    check_error("Splicing stdin from PTY", error);

    g_main_loop_run (loop);

    return 0;
}
