const Format = imports.format;
const Gio = imports.gi.Gio;
const GLib = imports.gi.GLib;
const Lang = imports.lang;
const Params = imports.params;
const Signals = imports.signals;

const BOOT_HARDDRIVE = 0;
const BOOT_NETWORK = 1;
const BOOT_IMAGE = 2;

const Machine = new Lang.Class({
    Name: 'Machine',

    _init: function(name, application, params) {
        params = Params.parse(params,
                              {
                                  drives: [],
                                  memory: '512M',
                                  bootType: BOOT_HARDDRIVE,
                                  privateHWAddr: null,
                                  privateListen: true,
                                  publicHWAddr: null,
                                  publicForwards: [],
                                  extraSerial: false,
                                  passSerial: [],
                                  spicePassword: null,
                                  spicePort: null,
                                  virtfs: []
                              });

        this.application = application;
        this.name = name;
        Lang.copyProperties(params, this);
        this._pending = {};
        this._commandSerial = 1;
        this.serialDevices = {};
    },

    run: function() {
        let args = [
            'qemu-kvm', '-name', this.name,
            '-m', this.memory,
            '-spice', 'port='+this.spicePort+',password='+this.spicePassword,
            '-chardev', 'stdio,id=mntr',
            '-mon', 'chardev=mntr,mode=control',
            '-vga', 'qxl',
            '-usbdevice', 'tablet', // Having an absolute device removes the need for mouse grabbing
        ];

        if (this.privateHWAddr)
            args.push.apply(args, this.application.networkManager.getNetworkArgs(0, this.privateHWAddr, this.privateListen));

        if (this.publicHWAddr) {
            let forwards = '';
            for (let forward of this.publicForwards) {
                forwards += ',guestfwd=' + forward;
            }
            args.push('-net', 'nic,vlan=1,macaddr='+this.publicHWAddr, '-net', 'user,vlan=1' + forwards);
        }

        if (this.bootType == BOOT_NETWORK)
            args.push('-boot', 'order=n');
        else
            args.push('-boot', 'order=c');

        args.push('-serial', 'pty');

        if (this.extraSerial)
            args.push('-serial', 'pty');

        for (let serial of this.passSerial)
            args.push('-serial', serial);

        for (let drive of this.drives)
            args.push('-drive','file='+drive.file+',if=virtio'+(drive.readonly ? ',readonly' : ''));

        for (let virtfs of this.virtfs)
            args.push('-virtfs', 'local,path=' + virtfs.path + ',mount_tag=' + virtfs.mountTag + ',security_model=none');

        this.subprocess = Gio.Subprocess.new(args, Gio.SubprocessFlags.STDIN_PIPE | Gio.SubprocessFlags.STDOUT_PIPE);

        let datastream = Gio.DataInputStream.new(this.subprocess.get_stdout_pipe());
        datastream.read_line_async(GLib.PRIORITY_DEFAULT, null, this._onLineRead.bind(this));
    },

    hardOff: function() {
        this._send('quit');
    },

    requestOff: function() {
        this._send('system_powerdown');
    },

    initialized: function() {
        this.emit('initialized');
    },

    _send: function(command, args, onReply) {
        let json = {
            execute: command
        };
        if (args != null)
            json['arguments'] = args;

        if (onReply != null) {
            json['id'] = String(this._commandSerial);
            this._commandSerial += 1;
            this._pending[json['id']] = onReply;
        }

        let str = JSON.stringify(json);
        // print("sending " + str);
        this.subprocess.get_stdin_pipe().write_bytes(new GLib.Bytes(str + '\n'), null);
    },

    _onLineRead: function(datastream, result) {
        let [line,] = datastream.read_line_finish_utf8(result);
        if (line == null) {
            this.subprocess.get_stdin_pipe().close(null);
            this.subprocess.wait(null);
            this.emit('exit');
        } else {
            let json = JSON.parse(line);
            // print(JSON.stringify(json));
            if ('QMP' in json) {
                this._send('qmp_capabilities');
                this._send('query-chardev', null, function(response) {
                    for (let device of response) {
                        this.serialDevices[device.label] = device.filename;
                    }
                    this.initialized();
                }.bind(this));
            } else if ('return' in json) {
                if ('id' in json) {
                    if (json.id in this._pending) {
                        this._pending[json.id](json['return']);
                        delete this._pending[json.id];
                    }
                }
            } else if ('error' in json) {
                if ('id' in json)
                    delete this._pending[json.id];
            }

            datastream.read_line_async(GLib.PRIORITY_DEFAULT, null, this._onLineRead.bind(this));
        }
    }
});
Signals.addSignalMethods(Machine.prototype);

const TargetMachine = new Lang.Class({
    Name: 'TargetMachine',
    Extends: Machine,

    _init: function(application, bootType) {
        let drives = [];
        if (bootType == BOOT_NETWORK || bootType == BOOT_IMAGE)
            drives.push({
                file: application.getHomeFile('images/local/gnome-continuous-x86_64-hwtest.qcow2').get_path(),
                readonly: true
            });
        drives.push({
            file: application.getHomeFile('target.image').get_path(),
        });

        this.parent('hwtest-target', application,
                    { spicePort: 3500,
                      spicePassword: 'bluefish',
                      privateHWAddr: '52:54:00:12:34:57',
                      privateListen: false,
                      bootType: bootType,
                      drives: drives,
                      memory: '1024M'
                    });
    }
});

const ControllerMachine = new Lang.Class({
    Name: 'ControllerMachine',
    Extends: Machine,

    _init: function(application, bootType) {

        let drives = [];
        if (bootType == BOOT_NETWORK || bootType == BOOT_IMAGE)
            drives.push({
                file: application.getHomeFile('images/local/gnome-continuous-x86_64-hwtest.qcow2').get_path(),
                readonly: true
            });
        drives.push({
            file: application.getHomeFile('controller.image').get_path(),
        });

        let srvGnomeImage = application.getHomeFile('srv-gnome-hwtest.image');
        if (srvGnomeImage.query_exists(null)) {
            drives.push({
                file: application.getHomeFile('srv-gnome-hwtest.image').get_path(),
            });
        }

        let params = { spicePort: 3501,
                       spicePassword: 'redfish',
                       privateHWAddr: '52:54:00:12:34:56',
                       publicHWAddr: '52:54:00:12:34:58',
                       publicForwards: [
                           'tcp:10.0.2.100:80-cmd:socat stdio tcp:127.0.0.1:' + application.httpdPort,
                           'tcp:10.0.2.101:80-cmd:socat stdio tcp:127.0.0.1:8000',
                       ],
                       extraSerial: true,
                       bootType: bootType,
                       drives: drives
                     };

        let settings = new Gio.Settings({ schema: 'org.gnome.HWTestSimulator' });
        let passSerial = settings.get_string("passthrough-serial");
        if (passSerial != '') {
            if (!Gio.File.new_for_path(passSerial).query_exists(null)) {
                printerr("Cannot find passthrough serial device: " + passSerial);
            } else {
                params.passSerial = [passSerial];
            }
        }

        let integrationDir = application.getHomeFile('overrides/gnome-hwtest');
        if (integrationDir.query_exists(null)) {
            params.virtfs = [
                {
                    mountTag: 'gnome-hwtest',
                    path: integrationDir.get_path()
                }
            ];
        }

        this.parent('hwtest-target', application, params);
    },

    initialized: function() {
        let m = /pty:(.*)/.exec(this.serialDevices['serial1']);
        let ptyfile = Gio.File.new_for_path(m[1]);

        let stty = Gio.Subprocess.new(['stty', '-F', m[1], 'raw', '-echo'], Gio.SubprocessFlags.NONE);
        stty.wait (null);

        this.powerin = Gio.DataInputStream.new(ptyfile.read(null));
        this.powerin.read_line_async(GLib.PRIORITY_DEFAULT, null,
                                     this._onSerialLine.bind(this));

        this.powerout = Gio.DataOutputStream.new(ptyfile.append_to(Gio.FileCreateFlags.NONE, null));

        this.parent();
    },

    _sendPower: function(str) {
        /* FIXME: could block */
        this.powerout.put_string(str, null);
        this.powerout.flush(null);
    },

    _sendPowerUsage: function() {
        this._sendPower("err usage: on <n>|off <n>|query|ping <token>\n");
    },

    _onPowerCommand: function(words) {
        let cmd = words[0].toLowerCase();
        if (cmd == 'on' || cmd == 'off') {
            if (words.length != 2 || String(parseInt(words[1])) != words[1]) {
                this._sendPowerUsage();
                return;
            }
            let which = parseInt(words[1]);
            if (which != 1) {
                this._sendPower("err " + which + " is out of range\n")
                return;
            }
            if (cmd == 'on')
                this.application.targetProxy.setAutoOn(true);
            else
                this.application.targetProxy.setAutoOn(false);

            this._sendPower("ok\n")
        } else if (cmd == 'ping') {
            if (words.length != 2) {
                this._sendPowerUsage();
                return;
            }
            this._sendPower("ok " + words[1] + "\n");
        } else if (cmd == 'query') {
            if (words.length != 1) {
                this._sendPowerUsage();
                return;
            }
            this._sendPower("ok " + (this.application.targetProxy.on ? '1' : '0') + "\n");
        } else {
            this._sendPowerUsage();
            return;
        }
    },

    _onSerialLine: function(powerin, result) {
        let line;
        try {
            [line,] = powerin.read_line_finish_utf8(result);
        } catch (e) {
            // We seem to get an I/O error rather than a clean EOF
            print("Got error");
            this.powerin.close(null);
            this.powerout.close(null);
            return;
        }
        if (line == null) {
            print("Got EOF");
            this.powerin.close(null);
            this.powerout.close(null);
        } else {
            this._onPowerCommand(line.trim().split(/\s+/));

            powerin.read_line_async(GLib.PRIORITY_DEFAULT, null,
                                    this._onSerialLine.bind(this));
        }
    }
});

const MACHINE_OFF = 0;
const MACHINE_POWERING_ON = 1;
const MACHINE_ON = 2;
const MACHINE_POWERING_OFF = 3;

const MODE_OFF = 0;
const MODE_AUTO = 1;
const MODE_ON = 2;

const MachineProxy = new Lang.Class({
    Name: 'MachineProxy',

    _init: function(application, name, construct) {
        this.application = application;
        this.name = name;
        this.construct = construct;
        this.machine = null;
        this.window = null;
        this.hasAuto = true;
        this.state = MACHINE_OFF;
        this.mode = MODE_AUTO;
        this.bootType = BOOT_HARDDRIVE;
        this.autoOn = false;
        this.on = false;
    },

    setHasAuto: function(hasAuto) {
        this.hasAuto = hasAuto;

        if (this.mode == MODE_AUTO && !hasAuto)
            this.mode = this.on ? MODE_ON : MODE_OFF;
    },

    setMode: function(mode) {
        if (mode == MODE_AUTO && !this.hasAuto)
            return;

        if (mode == this.mode)
            return;

        this.mode = mode;
        if (this.hasPower())
            this.boot();
        else
            this.hardOff();

        this.emit('mode-changed');
    },

    setAutoOn: function(on) {
        if (on == this.autoOn)
            return;

        this.autoOn = on;
        if (this.hasPower())
            this.boot();
        else
            this.hardOff();

        this.emit('auto-on-changed');
    },

    hasPower: function() {
        return (this.mode == MODE_ON || (this.mode == MODE_AUTO && this.autoOn));
    },

    boot: function() {
        if (!this.hasPower())
            return;

        if (this.on)
            return;

        this.on = true;

        if (this.state != MACHINE_OFF)
            return;

        this.state = MACHINE_POWERING_ON;
        this.machine = this.construct(this.bootType);

        this.machine.connect('initialized',
                             this._onInitialized.bind(this));
        this.machine.connect('exit',
                             this._onExit.bind(this));
        this.machine.run();

        this.emit('state-changed');
    },

    requestOff: function() {
        if (this.state == MACHINE_ON)
            this.machine.requestOff();
    },

    hardOff: function() {
        if (!this.on)
            return;

        this.on = false;

        if (this.state != MACHINE_ON)
            return;

        this.state = MACHINE_POWERING_OFF;
        this.machine.hardOff();
        this.emit('state-changed');
    },

    _onInitialized: function(machine) {
        this.state = MACHINE_ON;
        if (!this.on) {
            /* Turned off during initialization */
            this.on = true;
            this.hardOff();
        }

        this.emit('state-changed');
    },

    _onExit: function(machine) {
        /* Catch the case where we are turned back on during power-off */
        let needBoot = false;
        if (this.state == MACHINE_POWERING_OFF && this.on)
            needBoot = true;

        this.state = MACHINE_OFF;
        this.machine = null;
        this.on = false;
        this.emit('state-changed');

        if (needBoot)
            this.boot();
    },

    setBootType: function(bootType) {
        this.bootType = bootType;
    }
});
Signals.addSignalMethods(MachineProxy.prototype);
