const Format = imports.format;
const Gio = imports.gi.Gio;
const GLib = imports.gi.GLib;
const Gtk = imports.gi.Gtk;
const Lang = imports.lang;
const Mainloop = imports.mainloop;
const Params = imports.params;
const Signals = imports.signals;

const Machine = imports.machine;
const Network = imports.network;
const Spice = imports.spice;

const Application = new Lang.Class({
    Name: 'Application',
    Extends: Gtk.Application,

    _init: function() {
        this.parent();

        Gtk.Settings.get_default().set_property('gtk-application-prefer-dark-theme', true);

        let settings = new Gio.Settings({ schema: 'org.gnome.HWTestSimulator' });
        let gnomeContinuousHome = settings.get_string("gnome-continuous-home");
        gnomeContinuousHome = gnomeContinuousHome.replace(/^~(?=\/|$)/, GLib.get_home_dir());
        this.gnomeContinuousHome = Gio.File.new_for_path(gnomeContinuousHome);

        this.mainWindow = null;
        this.targetProxy = null;
        this.exitRequested = false;
    },

    getHomeFile: function(relative) {
        return this.gnomeContinuousHome.get_child(relative);
    },

    requestExit: function() {
        this.exitRequested = true;
        if (this.controllerProxy.machine)
            this.controllerProxy.machine.requestOff();
        if (this.targetProxy.machine)
            this.targetProxy.machine.requestOff();
        this._checkExit();
    },

    vfunc_activate: function() {
        if (!this.gnomeContinuousHome.query_exists(null) ||
            !this.getHomeFile('manifest.json').query_exists(null)) {
            let dialog = new Gtk.MessageDialog({
                message_type: Gtk.MessageType.ERROR,
                text: "GNOME Continuous build directory messing",
                secondary_text: Format.vprintf("'%s' does not seem to be a GNOME Continuous build directory. See https://wiki.gnome.org/Projects/HardwareTesting/Tasks/HackingEnvironment",
                                               [this.gnomeContinuousHome.get_path()]),
                buttons: Gtk.ButtonsType.OK
            });
            dialog.application = this;
            dialog.show_all();
            dialog.connect('response',
                           function() { dialog.destroy() });
        }

        if (this.mainWindow == null) {
            this.networkManager = new Network.NetworkManager();

            this.mainWindow = new Gtk.Window();
            this.mainWindow.application = this;
            this.mainWindow.set_default_size(1024, 600);

            let hbox = new Gtk.Box({ orientation: Gtk.Orientation.HORIZONTAL });
            this.mainWindow.add(hbox);

            let vbox = new Gtk.Box({ orientation: Gtk.Orientation.VERTICAL });
            hbox.pack_start(vbox, false, false, 0);

            this.headerBar = new Gtk.HeaderBar();
            this.headerBar.set_title("GNOME Hardware Testing");
            this.headerBar.set_show_close_button(true);
            this.mainWindow.set_titlebar(this.headerBar);

            this.serialBox = new Gtk.Box({ orientation: Gtk.Orientation.HORIZONTAL,
                                           homogeneous: true });
            this.serialBox.get_style_context().add_class("linked");
            this.headerBar.pack_end(this.serialBox);

            let button;
            button = new Gtk.RadioButton({ label: 'Console', draw_indicator: false,  });
            button.set_active(true);
            button.connect('toggled', this._onSerialToggled.bind(this, false));
            this.serialBox.pack_start(button, true, true, 0);

            button = new Gtk.RadioButton({ label: 'Serial', draw_indicator: false, group: button });
            button.connect('toggled', this._onSerialToggled.bind(this, true));
            this.serialBox.pack_start(button, true, true, 0);

            this.mainView = new Spice.MainView(this);
            this.mainView.connect('display-changed', this._onDisplayChanged.bind(this));
            this.mainView.connect('show-serial-changed', this._onShowSerialChanged.bind(this));
            hbox.pack_start(this.mainView, true, true, 0);

            this.controllerProxy = new Machine.MachineProxy(this,
                                                            "Controller",
                                                            function(bootType) {
                                                                return new Machine.ControllerMachine(this, bootType);
                                                            }.bind(this));
            this.controllerProxy.setHasAuto(false);
            this.controllerProxy.connect('state-changed',
                                          this._checkExit.bind(this));
            vbox.pack_start(new Spice.MachineWidget(this.mainView, this.controllerProxy),
                            false, false, 0);

            this.targetProxy = new Machine.MachineProxy(this,
                                                        "Target",
                                                        function(bootType) {
                                                            return new Machine.TargetMachine(this, bootType);
                                                        }.bind(this));
            this.targetProxy.setBootType(Machine.BOOT_NETWORK);
            this.targetProxy.connect('state-changed',
                                     this._checkExit.bind(this));
            vbox.pack_start(new Spice.MachineWidget(this.mainView, this.targetProxy),
                            false, false, 0);

            this.mainWindow.connect('delete-event', function() {
                this.requestExit();
                return true;
            }.bind(this));

            this.mainWindow.show_all();
            this._onDisplayChanged();

            this._startHttpd();
        }
    },

    vfunc_shutdown: function() {
        if (this._httpd)
            this._httpd.send_signal(15, null); /* SIGTERM */
        this.parent();
    },

    _checkExit: function(proxy) {
        if (this.exitRequested) {
            if (this.targetProxy.state == Machine.MACHINE_OFF &&
                this.controllerProxy.state == Machine.MACHINE_OFF)
                this.quit();
        }
    },

    _onDisplayChanged: function() {
        let haveMachine = this.mainView.machineWidget != null;
        this.serialBox.set_sensitive(haveMachine);

        if (haveMachine)
            this.headerBar.set_subtitle(this.mainView.machineWidget.proxy.name);
        else
            this.headerBar.set_subtitle('');
    },

    _onShowSerialChanged: function() {
        let buttons = this.serialBox.get_children();
        if (this.mainView.showSerial)
            buttons[1].set_active(true);
        else
            buttons[0].set_active(true);
    },

    _onSerialToggled: function(showSerial, button) {
        if (button.active) {
            this.mainView.setShowSerial(showSerial);
        }
    },

    _startHttpd: function() {
        this._httpd = Gio.Subprocess.new(['ostree', 'trivial-httpd', '--port-file=-',
                                          this.gnomeContinuousHome.get_path()],
                                        Gio.SubprocessFlags.STDOUT_PIPE);
        let datastream = Gio.DataInputStream.new(this._httpd.get_stdout_pipe());
        datastream.read_line_async(GLib.PRIORITY_DEFAULT, null, function(datastream, result) {
            let [line,] = datastream.read_line_finish_utf8(result);
            this.httpdPort = Number(line);
        }.bind(this));
    }
});

function main() {
    let app = new Application();

    GLib.unix_signal_add(GLib.PRIORITY_DEFAULT, 2, function() {
        printerr("Interrupted");
        app.requestExit();
    });

    app.run([]);

    return true;
}

main() ? 0 : 1;
